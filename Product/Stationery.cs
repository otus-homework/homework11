﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework11
{
    public class Stationery : Product
    {
        public Stationery(string name) : base(name, "Канцтовары")
        {
        }

        public override Product MyClone()
        {
            return new Stationery(Name);
        }
    }
}

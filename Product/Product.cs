﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework11
{
    public class Product : IMyCloneable<Product>, ICloneable
    {
        private readonly string _type;
        public string Name;

        public Product(string name, string type)
        {
            Name = name;
            _type = type;
        }
        public override string ToString()
        {
            return $"Товар: {Name}, тип товара: {_type}";
        }


        public virtual Product MyClone()
        {
            return new Product(Name, _type);
        }

        public object Clone()
        {
            return MyClone();
        }
    }
}

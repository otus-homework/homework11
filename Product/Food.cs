﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework11
{
    public class Food : Product
    {
        public Food(string name) : base(name, "Питание")
        {
        }
        public override Product MyClone()
        {
            return new Food(Name);
        }
    }
}

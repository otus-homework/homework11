﻿namespace Homework11
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}
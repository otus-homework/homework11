﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework11
{
    public class Employee:IMyCloneable<Employee>, ICloneable
    {
        private readonly string _position;
        public string Name { get; set; }

        public Employee(string name, string position)
        {
            Name = name;
            _position = position;
        }

        public override string ToString()
        {
            return $"Сотрудник: {Name}, должность: {_position}";
        }

        public virtual Employee MyClone()
        {
            return new Employee(Name, _position);
        }

        public object Clone()
        {
            return MyClone();
        }
    }
}

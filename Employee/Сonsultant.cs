﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework11
{
    public class Сonsultant : Employee
    {
        public Сonsultant(string name) : base(name, "Консультант")
        {
        }

        public override Employee MyClone()
        {
            return new Сonsultant(Name);
        }

    }
}

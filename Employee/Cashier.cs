﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework11
{
    public class Cashier : Employee
    {
        public Cashier(string name) : base(name, "Кассир")
        {
        }

        public override Employee MyClone()
        {
            return new Cashier(Name);
        }

    }
}

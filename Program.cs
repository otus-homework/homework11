﻿using System;

namespace Homework11
{
    class Program
    {
        static void Main(string[] args)
        {
            //ICloneable
            //+ стандартный интерфейс, простой в использовании
            //- возвращает object(boxing)
            //IMyCloneable<T>
            //+ generic (возвращает конкретный тип)
            //- принудильный upCust у дочерних классов
            Employee cashier1 = new Cashier("Иванова");
            Employee cashier2 = (Employee)cashier1.Clone();
            cashier2.Name = "Петрова";
            Console.WriteLine(cashier1);
            Console.WriteLine(cashier2);

            Employee consultant1 = new Сonsultant("Анисимов");
            Employee consultant2 = consultant1.MyClone();
            consultant2.Name = "Нагиев";
            Console.WriteLine(consultant1);
            Console.WriteLine(consultant2);

            Product food1 = new Food("Хлебушек");
            Product food2 = (Product)food1.Clone();
            food2.Name = "Печеньки";
            Console.WriteLine(food1);
            Console.WriteLine(food2);

            Product stationery1 = new Stationery("Ручка");
            Product stationery2 = stationery1.MyClone();
            stationery2.Name = "Линейка";
            Console.WriteLine(stationery1);
            Console.WriteLine(stationery2);

            Console.ReadKey();
        }
    }
}
